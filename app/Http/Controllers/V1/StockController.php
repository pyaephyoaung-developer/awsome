<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Stock;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\StockResource;
use App\Http\Resources\StockCheckResource;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stocks =  Stock::where('amount', '<>', 0)->orderBy("name")->get();
        $response = [];
        foreach ($stocks as $stock) {
            $response[$stock->name] = $stock->amount;
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|alpha|max:8',
            'amount' => 'integer',
        ]);
        if ($validator->fails()) {
            return response()->json([
                "message" => implode(' ', $validator->errors()->all()),
            ], 400);
        }
        $field = collect($validator->validated())->filter()->all();
        
        $stock = Stock::where('name' , $field['name'])->first();
        if ($stock) {
            $stock->amount = isset($field['amount'])? $field['amount']: 0;
            $stock->save();
        } else {
            $stock = Stock::create($field);
            $stock->refresh();
        }
        return response()
            ->json(new StockResource($stock))
            ->header('Location', url('/v1/stocks/'.$stock->name));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $name
     * @return \Illuminate\Http\Response
     */
    public function show_name($name)
    {
        $stock = Stock::where('name', $name)->first();
        if (!$stock) {
            $stock = new Stock();
            $stock->name = $name;
            $stock->amount = 0;
        }
        return response()
            ->json(new StockCheckResource($stock));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destory_all()
    {
        Stock::whereNotNull('id')->delete();
    }
}

