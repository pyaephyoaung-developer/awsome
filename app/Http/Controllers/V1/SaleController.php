<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Stock;
use App\Models\Sale;
use App\Http\Resources\SaleResource;
use Illuminate\Support\Facades\Validator;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|alpha|max:8',
            'amount' => 'integer',
            'price' => 'integer',
        ]);
        if ($validator->fails()) {
            return response()->json([
                "message" => implode(' ', $validator->errors()->all()),
            ], 400);
        }
        $data = collect($validator->validated())->filter()->all();
        $stock = Stock::where('name', $data['name'])->first();
        if (!$stock) {
            return response()->json([
                "message" => 'Stock '.$data['name']. ' does not exists',
            ], 400);
        }
        if (isset($data['amount'])) {
            $stock->amount -= $data['amount'];
        } else {
            $stock->amount -= 1 ;
        }
        $stock->save();
        $sale = Sale::create($data);
        $sale->refresh();
        $sale->sales = $sale->amount * $sale->price;
        $sale->save();
        return response()
            ->json(new SaleResource($sale))
            ->header('Location', url('/v1/sales/'.$sale->name));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $name
     * @return \Illuminate\Http\Response
     */
    public function show_name($name)
    {
        $sale = Sale::firstOrNew([
            'name' => $name
        ],[
            'amount' => 0,
            'price' => 0,
        ]);
        return response()
            ->json(new SaleResource($sale));
    }

    /**
     * Display the sales result
     *
     * @return \Illuminate\Http\Response
     */
    public function sales()
    {
        $sales = Sale::sum('sales');

        $response = [
            'sales' => number_format($sales, 2, '.', ''),
        ];
        return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destory_all()
    {
        Sale::whereNotNull('id')->delete();
    }
}
