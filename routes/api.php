<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\V1\StockController;
use App\Http\Controllers\V1\SaleController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//v1
//TASK3 -> 1 在庫の更新、作成
Route::post('/v1/stocks', [StockController::class, 'create']);
//TASK3 -> 3
Route::get('/v1/stocks', [StockController::class, 'index']);
//TASK3 -> 3
Route::get('/v1/stocks/{name?}', [StockController::class, 'show_name']);
//TASK3 -> 3
Route::post('/v1/sales/', [SaleController::class, 'create']);
//TASK3 -> 4
Route::get('/v1/sales/', [SaleController::class, 'sales']);
//TASK3 -> 3
Route::get('/v1/sales/{name?}', [SaleController::class, 'show_name']);
//TASK3 -> 5
Route::delete('/v1/stocks', [StockController::class, 'destory_all']);


